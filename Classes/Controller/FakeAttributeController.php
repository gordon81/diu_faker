<?php
namespace Diu\DiuFaker\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>, di-unternehmer
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FakeAttributeController
 */
class FakeAttributeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $fakeAttributes = $this->fakeAttributeRepository->findAll();
        $this->view->assign('fakeAttributes', $fakeAttributes);
    }
    
    /**
     * action show
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute
     * @return void
     */
    public function showAction(\Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute)
    {
        $this->view->assign('fakeAttribute', $fakeAttribute);
    }
    
    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        
    }
    
    /**
     * action create
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeAttribute $newFakeAttribute
     * @return void
     */
    public function createAction(\Diu\DiuFaker\Domain\Model\FakeAttribute $newFakeAttribute)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->fakeAttributeRepository->add($newFakeAttribute);
        $this->redirect('list');
    }
    
    /**
     * action edit
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute
     * @ignorevalidation $fakeAttribute
     * @return void
     */
    public function editAction(\Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute)
    {
        $this->view->assign('fakeAttribute', $fakeAttribute);
    }
    
    /**
     * action update
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute
     * @return void
     */
    public function updateAction(\Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->fakeAttributeRepository->update($fakeAttribute);
        $this->redirect('list');
    }
    
    /**
     * action delete
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute
     * @return void
     */
    public function deleteAction(\Diu\DiuFaker\Domain\Model\FakeAttribute $fakeAttribute)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->fakeAttributeRepository->remove($fakeAttribute);
        $this->redirect('list');
    }

}