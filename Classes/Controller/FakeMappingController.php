<?php
namespace Diu\DiuFaker\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>, di-unternehmer
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FakeMappingController
 */
class FakeMappingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * fakeMappingRepository
     *
     * @var \Diu\DiuFaker\Domain\Repository\FakeMappingRepository
     * @inject
     */
    protected $fakeMappingRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $fakeMappings = $this->fakeMappingRepository->findAll();
        $this->view->assign('fakeMappings', $fakeMappings);
    }
    
    /**
     * action show
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping
     * @return void
     */
    public function showAction(\Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping)
    {
        $this->view->assign('fakeMapping', $fakeMapping);
    }
    
    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        
    }
    
    /**
     * action create
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeMapping $newFakeMapping
     * @return void
     */
    public function createAction(\Diu\DiuFaker\Domain\Model\FakeMapping $newFakeMapping)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->fakeMappingRepository->add($newFakeMapping);
        $this->redirect('list');
    }
    
    /**
     * action edit
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping
     * @ignorevalidation $fakeMapping
     * @return void
     */
    public function editAction(\Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping)
    {
        $this->view->assign('fakeMapping', $fakeMapping);
    }
    
    /**
     * action update
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping
     * @return void
     */
    public function updateAction(\Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->fakeMappingRepository->update($fakeMapping);
        $this->redirect('list');
    }
    
    /**
     * action delete
     *
     * @param \Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping
     * @return void
     */
    public function deleteAction(\Diu\DiuFaker\Domain\Model\FakeMapping $fakeMapping)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->fakeMappingRepository->remove($fakeMapping);
        $this->redirect('list');
    }
    
    /**
     * action build
     *
     * @return void
     */
    public function buildAction()
    {
        
    }
    
    /**
     * action analyse
     *
     * @return void
     */
    public function analyseAction()
    {
        
    }

}