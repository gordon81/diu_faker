<?php
namespace Diu\DiuFaker\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>, di-unternehmer
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FakeAttribute
 */
class FakeAttribute extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{

    /**
     * attribute
     *
     * @var string
     * @validate NotEmpty
     */
    protected $attribute = '';
    
    /**
     * locale
     *
     * @var int
     */
    protected $locale = 0;
    
    /**
     * provider
     *
     * @var int
     * @validate NotEmpty
     */
    protected $provider = 0;
    
    /**
     * fakefunktion
     *
     * @var int
     * @validate NotEmpty
     */
    protected $fakefunktion = 0;
    
    /**
     * functionproperty
     *
     * @var string
     */
    protected $functionproperty = '';
    
    /**
     * Returns the attribute
     *
     * @return string $attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
    
    /**
     * Sets the attribute
     *
     * @param string $attribute
     * @return void
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }
    
    /**
     * Returns the fakefunktion
     *
     * @return int $fakefunktion
     */
    public function getFakefunktion()
    {
        return $this->fakefunktion;
    }
    
    /**
     * Sets the fakefunktion
     *
     * @param int $fakefunktion
     * @return void
     */
    public function setFakefunktion($fakefunktion)
    {
        $this->fakefunktion = $fakefunktion;
    }
    
    /**
     * Returns the locale
     *
     * @return int $locale
     */
    public function getLocale()
    {
        return $this->locale;
    }
    
    /**
     * Sets the locale
     *
     * @param int $locale
     * @return void
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
    
    /**
     * Returns the provider
     *
     * @return int $provider
     */
    public function getProvider()
    {
        return $this->provider;
    }
    
    /**
     * Sets the provider
     *
     * @param int $provider
     * @return void
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }
    
    /**
     * Returns the functionproperty
     *
     * @return string $functionproperty
     */
    public function getFunctionproperty()
    {
        return $this->functionproperty;
    }
    
    /**
     * Sets the functionproperty
     *
     * @param string $functionproperty
     * @return void
     */
    public function setFunctionproperty($functionproperty)
    {
        $this->functionproperty = $functionproperty;
    }

}