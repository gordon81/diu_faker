<?php
namespace Diu\DiuFaker\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>, di-unternehmer
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Diu\DiuFaker\Controller\FakeAttributeController.
 *
 * @author Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>
 */
class FakeAttributeControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \Diu\DiuFaker\Controller\FakeAttributeController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('Diu\\DiuFaker\\Controller\\FakeAttributeController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllFakeAttributesFromRepositoryAndAssignsThemToView()
	{

		$allFakeAttributes = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$fakeAttributeRepository = $this->getMock('', array('findAll'), array(), '', FALSE);
		$fakeAttributeRepository->expects($this->once())->method('findAll')->will($this->returnValue($allFakeAttributes));
		$this->inject($this->subject, 'fakeAttributeRepository', $fakeAttributeRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('fakeAttributes', $allFakeAttributes);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenFakeAttributeToView()
	{
		$fakeAttribute = new \Diu\DiuFaker\Domain\Model\FakeAttribute();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('fakeAttribute', $fakeAttribute);

		$this->subject->showAction($fakeAttribute);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenFakeAttributeToFakeAttributeRepository()
	{
		$fakeAttribute = new \Diu\DiuFaker\Domain\Model\FakeAttribute();

		$fakeAttributeRepository = $this->getMock('', array('add'), array(), '', FALSE);
		$fakeAttributeRepository->expects($this->once())->method('add')->with($fakeAttribute);
		$this->inject($this->subject, 'fakeAttributeRepository', $fakeAttributeRepository);

		$this->subject->createAction($fakeAttribute);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenFakeAttributeToView()
	{
		$fakeAttribute = new \Diu\DiuFaker\Domain\Model\FakeAttribute();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('fakeAttribute', $fakeAttribute);

		$this->subject->editAction($fakeAttribute);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenFakeAttributeInFakeAttributeRepository()
	{
		$fakeAttribute = new \Diu\DiuFaker\Domain\Model\FakeAttribute();

		$fakeAttributeRepository = $this->getMock('', array('update'), array(), '', FALSE);
		$fakeAttributeRepository->expects($this->once())->method('update')->with($fakeAttribute);
		$this->inject($this->subject, 'fakeAttributeRepository', $fakeAttributeRepository);

		$this->subject->updateAction($fakeAttribute);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenFakeAttributeFromFakeAttributeRepository()
	{
		$fakeAttribute = new \Diu\DiuFaker\Domain\Model\FakeAttribute();

		$fakeAttributeRepository = $this->getMock('', array('remove'), array(), '', FALSE);
		$fakeAttributeRepository->expects($this->once())->method('remove')->with($fakeAttribute);
		$this->inject($this->subject, 'fakeAttributeRepository', $fakeAttributeRepository);

		$this->subject->deleteAction($fakeAttribute);
	}
}
