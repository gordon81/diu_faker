<?php

namespace Diu\DiuFaker\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>, di-unternehmer
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Diu\DiuFaker\Domain\Model\FakeAttribute.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Gordon Brüggemann <gordon.brueggemann@di-unternehmer.com>
 */
class FakeAttributeTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Diu\DiuFaker\Domain\Model\FakeAttribute
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Diu\DiuFaker\Domain\Model\FakeAttribute();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getAttributeReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAttribute()
		);
	}

	/**
	 * @test
	 */
	public function setAttributeForStringSetsAttribute()
	{
		$this->subject->setAttribute('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'attribute',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLocaleReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setLocaleForIntSetsLocale()
	{	}

	/**
	 * @test
	 */
	public function getProviderReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setProviderForIntSetsProvider()
	{	}

	/**
	 * @test
	 */
	public function getFakefunktionReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setFakefunktionForIntSetsFakefunktion()
	{	}

	/**
	 * @test
	 */
	public function getFunctionpropertyReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getFunctionproperty()
		);
	}

	/**
	 * @test
	 */
	public function setFunctionpropertyForStringSetsFunctionproperty()
	{
		$this->subject->setFunctionproperty('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'functionproperty',
			$this->subject
		);
	}
}
