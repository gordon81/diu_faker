<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Diu.' . $_EXTKEY,
		'tools',	 // Make module a submodule of 'tools'
		'createfake',	// Submodule key
		'',						// Position
		array(
			'FakeMapping' => 'list, show, new, create, edit, update, delete, build, analyse','FakeAttribute' => 'list, show, new, create, edit, update, delete',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_createfake.xlf',
		)
	);

}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Faker');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_diufaker_domain_model_fakemapping', 'EXT:diu_faker/Resources/Private/Language/locallang_csh_tx_diufaker_domain_model_fakemapping.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_diufaker_domain_model_fakemapping');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_diufaker_domain_model_fakeattribute', 'EXT:diu_faker/Resources/Private/Language/locallang_csh_tx_diufaker_domain_model_fakeattribute.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_diufaker_domain_model_fakeattribute');
